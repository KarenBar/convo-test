package example.com.convo.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import example.com.convo.MainActivity;
import example.com.convo.R;

/**
 * Created by karenbarreto on 07-10-15.
 */
public class SettingsFragment extends Fragment {


    private View parentView;
    private MainActivity parentActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            parentView = inflater.inflate(R.layout.fragment_main, container, false);
        }catch(InflateException e){

        }
        setUpViews(parentView);
        return parentView;
    }

    private void setUpViews(View v) {
        parentActivity = (MainActivity) getActivity();
    }



    public void onViewCreated(final View view, final Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        TextView text = (TextView) view.findViewById(R.id.section_label);
        text.setText("More");
    }
}
