package example.com.convo.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import java.util.ArrayList;
import example.com.convo.MainActivity;
import example.com.convo.R;
import example.com.convo.adapter.ContactListAdapter;
import example.com.convo.model.Contact;
import example.com.convo.ui.ExpandableHeightExpandableList;

/**
 * Created by karenbarreto on 07-10-15.
 */
public class ContactListFragment extends Fragment {

    private View parentView;
    private MainActivity parentActivity;
    private ExpandableHeightExpandableList mList;
    private EditText search;

    private ArrayList<Contact> mContactList = new ArrayList<Contact>() ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            parentView = inflater.inflate(R.layout.contact_list_fragment, container, false);
        }catch(InflateException e){

        }

        populateContactList();
        setUpViews(parentView);
        return parentView;
    }

    private void setUpViews(View v) {
        parentActivity = (MainActivity) getActivity();


    }



    public void onViewCreated(final View view, final Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        mList = (ExpandableHeightExpandableList) view.findViewById(R.id.listView);
        mList.setExpanded(true);

        mList.setAdapter(new ContactListAdapter(parentActivity, parentActivity.getLayoutInflater(), mContactList));


        mList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    mList.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        search = (EditText) view.findViewById(R.id.editText);
        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                int textlength = charSequence.length();
                ArrayList<Contact> tempArrayList = new ArrayList<Contact>();
                for(Contact c: mContactList){
                    if (textlength <= c.getName().length()) {
                        if (c.getName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            tempArrayList.add(c);
                        }
                    }
                }
                mList.setAdapter(new ContactListAdapter(parentActivity, parentActivity.getLayoutInflater(), tempArrayList));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void populateContactList() {

        ArrayList<String> one_phone_number = new ArrayList<String>();
        one_phone_number.add("+56 9 55555555 ");

        ArrayList<String> two_phone_number = new ArrayList<String>();
        two_phone_number.add("+56 9 55555555 ");
        two_phone_number.add("+56 2 55555555 ");

        ArrayList<String> tree_phone_number = new ArrayList<String>();
        tree_phone_number.add("+56 9 55555555 ");
        tree_phone_number.add("+56 2 55555555 ");
        tree_phone_number.add("+58 412 5555555 ");

        mContactList.add(new Contact("James",one_phone_number));
        mContactList.add(new Contact("John",two_phone_number));
        mContactList.add(new Contact("Robert",tree_phone_number));
        mContactList.add(new Contact("Michael",one_phone_number));
        mContactList.add(new Contact("Mary",two_phone_number));
        mContactList.add(new Contact("William",tree_phone_number));
        mContactList.add(new Contact("David",one_phone_number));
        mContactList.add(new Contact("Richard",two_phone_number));
        mContactList.add(new Contact("Charles",tree_phone_number));
        mContactList.add(new Contact("Joseph",one_phone_number));
        mContactList.add(new Contact("Thomas",two_phone_number));
        mContactList.add(new Contact("Patricia",tree_phone_number));
        mContactList.add(new Contact("Christopher",one_phone_number));
        mContactList.add(new Contact("Linda",two_phone_number));
        mContactList.add(new Contact("Barbara",tree_phone_number));
        mContactList.add(new Contact("Daniel",one_phone_number));
        mContactList.add(new Contact("Paul",two_phone_number));
        mContactList.add(new Contact("Mark",tree_phone_number));
        mContactList.add(new Contact("Elizabeth",one_phone_number));
        mContactList.add(new Contact("Donald",two_phone_number));


    }


}
