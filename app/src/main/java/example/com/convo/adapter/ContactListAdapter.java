package example.com.convo.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import example.com.convo.R;
import example.com.convo.model.Contact;
import example.com.convo.ui.RoundedTransformation;

/**
 * Created by karenbarreto on 07-10-15.
 */
public class ContactListAdapter extends BaseExpandableListAdapter {


    private Dialog dialog;
    private ArrayList<Contact> mList;
    private LayoutInflater mInflater;
    private Activity mActivity;
    private int[] bg = {R.mipmap.contact_iconlg_home, R.mipmap.contact_iconlg_work, R.mipmap.contact_iconlg_mobile};


    public ContactListAdapter(Activity activity, LayoutInflater inflater,ArrayList<Contact> list ) {

        this.mInflater=inflater;
        this.mList=list;
        mActivity = activity;

        dialog = new Dialog(mActivity, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    @Override
    public int getGroupCount() {
        return mList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return mList.get(i).getPhone_number().size();
    }

    @Override
    public Object getGroup(int i) {
        return mList.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return mList.get(i).getPhone_number().get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        view= null;
         if (view == null) {
             view = mInflater.inflate(R.layout.contact_list_item,null);
             ImageView home = (ImageView) view.findViewById(R.id.home);
             ImageView work = (ImageView) view.findViewById(R.id.work);
             ImageView mobile = (ImageView) view.findViewById(R.id.mobile);
             ImageView indicator = (ImageView) view.findViewById(R.id.indicator);
             ImageView info = (ImageView) view.findViewById(R.id.info);
             TextView name = (TextView) view.findViewById(R.id.textView);

             name.setText(mList.get(i).getName());

             if(mList.get(i).getPhone_number().size()==1){
                 home.setVisibility(View.VISIBLE);
             } else  if(mList.get(i).getPhone_number().size()==2){

                 home.setVisibility(View.VISIBLE);
                 work.setVisibility(View.VISIBLE);
             }else  if(mList.get(i).getPhone_number().size()==3){

                 home.setVisibility(View.VISIBLE);
                 work.setVisibility(View.VISIBLE);
                 mobile.setVisibility(View.VISIBLE);
             }

             if(b){
                 indicator.setImageResource(R.mipmap.contacts_drawer_collapse);
                 view.setBackgroundResource(R.mipmap.contact_drawer_tiletop);
             }else {
                 indicator.setImageResource(R.mipmap.contacts_drawer_expand);
                 view.setBackgroundResource(0);
             }



             info.setTag(i);
             info.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {

                     LayoutInflater mInflater = (LayoutInflater) mActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                     final View v = mInflater.inflate(R.layout.contact_info_dialog, null);

                     TextView name = (TextView) v.findViewById(R.id.name);
                     ImageView profile = (ImageView) v.findViewById(R.id.profile);
                     name.setText(mList.get(Integer.valueOf(view.getTag().toString())).getName().toUpperCase());

                     Picasso.with(mActivity)
                             .load("http://www.binarytradingforum.com/core/image.php?userid=27&dateline=1355305878")
                             .fit()
                             .transform(new RoundedTransformation(75, 0))
                             .into(profile);

                     dialog.setContentView(v);
                     dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                     dialog.show();
                 }
             });


        }

        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        view = null;
        if (view == null) {
            view = mInflater.inflate(R.layout.phone_number_item,null);

            ImageView icon = (ImageView) view.findViewById(R.id.imageView2);
            TextView number = (TextView) view.findViewById(R.id.textView);

            number.setText(mList.get(i).getPhone_number().get(i1));
            icon.setImageResource(bg[i1]);

            if(i1==mList.get(i).getPhone_number().size()-1){

                view.setBackgroundResource(R.mipmap.contact_drawer_tilebottom);
            }else {

                view.setBackgroundResource(R.mipmap.contact_drawer_tilemiddle);

            }

            view.setTag(i+";"+i1);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    LayoutInflater mInflater = (LayoutInflater) mActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    final View v = mInflater.inflate(R.layout.alert_dialog, null);

                    TextView name = (TextView) v.findViewById(R.id.name);
                    TextView phone = (TextView) v.findViewById(R.id.phone_number);
                    name.setText(mList.get(Integer.valueOf(view.getTag().toString().split(";")[0])).getName());
                    phone.setText(mList.get(Integer.valueOf(view.getTag().toString().split(";")[0])).getPhone_number().get(Integer.valueOf(view.getTag().toString().split(";")[1])));

                    dialog.setContentView(v);
                    dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                    dialog.show();
                }
            });

        }



        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
