package example.com.convo;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import example.com.convo.fragment.ContactListFragment;
import example.com.convo.fragment.DialFragment;
import example.com.convo.fragment.HistoryFragment;
import example.com.convo.fragment.NearbyFragment;
import example.com.convo.fragment.SettingsFragment;
import example.com.convo.model.Contact;
import example.com.convo.ui.PagerSlidingTabStrip;
import example.com.convo.widget.CameraPreview;

public class MainActivity extends FragmentActivity {

    private ViewPager mPager;
    private PagerSlidingTabStrip mTabs;
    private LinearLayout tabsContainer;
    private Camera mCamera;
    private CameraPreview mPreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPager = (ViewPager) findViewById(R.id.pager);
        mTabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        TabAdapter pagerAdapter = new TabAdapter(getSupportFragmentManager());
        mPager.setAdapter(pagerAdapter);
        mTabs.setViewPager(mPager);
        mTabs.selected(0);
        mCamera = getCameraInstance();
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class TabAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.CustomTabProvider {

        public TabAdapter(FragmentManager fm) {
            super(fm);
        }

        private int[] ICONS = {R.drawable.dial_selector, R.drawable.contact_list_selector, R.drawable.recent_selector, R.drawable.nearby_selector, R.drawable.more_selector};


        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new DialFragment();
                case 1:
                    return new ContactListFragment();
                case 2:
                    return new HistoryFragment();
                case 3:
                    return new NearbyFragment();
                case 4:
                    return new SettingsFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public View getCustomTabView(int position) {
            View tabView = LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_tab, mTabs, false);
            TextView tabTitle = (TextView) tabView.findViewById(R.id.tabView);
            tabTitle.setCompoundDrawablesWithIntrinsicBounds(0, ICONS[position], 0, 0);
            return tabView;
        }


    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    public void onSectionAttached(int number) {

    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
            c.setDisplayOrientation(90);
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

}
