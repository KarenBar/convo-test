package example.com.convo.model;

import java.util.ArrayList;

/**
 * Created by karenbarreto on 07-10-15.
 */
public class Contact {

    private String name;

    private ArrayList<String> phone_number;

    public ArrayList<String> getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(ArrayList<String> phone_number) {
        this.phone_number = phone_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Contact(String name, ArrayList<String> phone_number) {
        this.name = name;
        this.phone_number = phone_number;
    }
}
